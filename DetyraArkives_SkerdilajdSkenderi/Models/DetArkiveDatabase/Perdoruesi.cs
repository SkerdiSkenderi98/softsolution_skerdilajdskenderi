﻿/**
* Versioni: 1.0.0
* Data: 21/09/2020
* Programuesi: Skerdi Skenderi
* Pershkrimi: File permban metoda get/set per krijimin e nje tabele Perdoruesis ne databaze si dhe parametra autentikimi te perdoruesit.
**/
using DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DetyraArkives_SkerdilajdSkenderi.Models
{
    public class Perdoruesi
    {
        [Key]
        public int ID_Perdoruesi { get; set; }
        [Required]
        public string Emer { get; set; }
        
        [Required(ErrorMessage = "Email duhet plotesuar")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email nuk i ploteson kushtet")]
        public string Email { get; set; }
        
        [Required]
        [DataType(DataType.Password, ErrorMessage = "Fjalekalimi nuk i mbush kushtet")]
        public string Fjalekalim { get; set; }

    public List<Dokument> Dokuments { get; set; }
    }
}
