﻿/**
* Versioni: 1.0.0
* Data: 21/09/2020
* Programuesi: Skerdi Skenderi
* Pershkrimi: File permban metoda get/set per krijimin e nje tabele Dokument ne databaze, ne lidhje me klasat e Inspektime, LlojDokument, Subjekti dhe Tags
**/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase
{

    public class Dokument
    {
        [Key]
        public int ID_Dokument { get; set; }


        public virtual Perdoruesi Perdoruesi { get; set; }


        public string Emer { get; set; }
        public string Tipi { get; set; }
        public byte[] Permbajtja { get; set; }
        public string Zyra { get; set; }
        public int Kutia { get; set; }
        public int Rafti { get; set; }
        public DateTime DateRregjistirimi { get; set; }

        public List<Tags> Tags { get; set; }
        public Subjekti Subjekti { get; set; }
        public Inspektime Inspektime { get; set; }
        public LlojDokument LlojDokument { get; set; }

    }
}

