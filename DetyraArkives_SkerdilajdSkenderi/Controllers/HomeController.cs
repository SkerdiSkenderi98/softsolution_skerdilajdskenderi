﻿/**
* Versioni: 1.0.0
* Data: 17/09/2020
* Programuesi:  Skerdi Skenderi
* Pershkrimi: Klase kontroller per menaxhimin e shumices se funksionaliteteve te arkives (funksionalitetet qe kane te bejne me faqjen kryesore te arkives)
**/

using Antlr.Runtime.Misc;
using DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase;
using System.IO.Compression;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace DetyraArkives_SkerdilajdSkenderi.Controllers
{
    /**
    * Data: 24/09/2020
    * Programuesi: Skerdi Skenderi
    * Klasa: ArchiveControlller
    * Arsyeja:Menaxhon kerkesat qe fijne nga Index.cshtml ne 
    * Trashegon nga: Controller
    **/
    public class HomeController : Controller
    {
        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda:Index
        * Arsyeja:Menaxhon kerkesen e login si dhe kerkesen e view per Home
        * Pershkrimi:Vendoset modeli i subjekteve dhe afishohen ne Index view.
        * Return: Kthen View (index.cshtml)
        **/
        public ActionResult Index()
        {
            using (var db = new DetArkiveDatabaseContext())
            {
                var subjektet = db.Subjekt.ToList();
                dynamic model = new ExpandoObject();
                model.subjektet = subjektet;
                return View(model);
            }

        }

        /**
         * Data: 18/11/2020
         * Programuesi: Skerdi Skenderi
         * Metoda: Register
         * Arsyeja: Per te mundesuar navigimin ne faqjen e Rregjistrimit
         * Pershkrimi: Marrin kerkesen nga nje Url.Action dhe afishon faqjen e Rregjistrimit.
         * Return: View perkatese (Register)
         **/
        public ActionResult Register()
        {
            ViewBag.Message = "Register Page";

            return View();
        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: Login
        * Arsyeja: Per te mundesuar navigimin ne faqjen e Login
        * Pershkrimi:Marrin kerkesen nga nje Url.Action dhe afishon faqjen e Login.
        * Return: View perkatese (Login)
        **/

        public ActionResult Login()
        {
            ViewBag.Message = "Login";

            return View();
        }

        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: Files
        * Arsyeja: Per te afishuar skedaret e pare te subjekteve mbasi perdoruesi logohet ne arkive per here te pare.
        * Pershkrimi:Krijohet ExpandoObject ku vendoset modeli i subjekteve dhe dokumenta te cilat kalohen dhe afishohen ne Index view.
        * Return: View perkatese (Login)
        **/
        public ActionResult Files()
        {
            dynamic skedareview = new ExpandoObject();
            var subjektet = new List<Subjekti>();
            using (var partialviewskedare = new DetArkiveDatabaseContext())
            {
                skedareview.Subjekti = partialviewskedare.Subjekt.ToList();
                skedareview.LlojDokument = partialviewskedare.LlojDokuments.ToList();
            }
            if (Session["Emer"] != null)
            {

                return View(skedareview);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult FilesPartialRender()
        {
            DetArkiveDatabaseContext db = new DetArkiveDatabaseContext();


            return View(from Subjekti in db.Subjekt.Take(20000).ToList() select Subjekti);

        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: Inspektime
        * Arsyeja: Per te afishuar partialView per skedaret e Inspektimeve te subjekteve perkates.
        * Pershkrimi: Merret klasa model e subjektit dhe Inspektime. I kalon ne nje liste dhe i afishon ne partial view perkatese.
        * Parametrat:string subjekti
        * Return: PartialView bazuar ne model dinamik. 
        **/

        public ActionResult Inspektime(string subjekti)
        {
            using (var ac = new DetArkiveDatabaseContext())
            {
                var dokumenta = ac.Subjekt.Include(s => s.Inspektimes).Where(s => s.Emer == subjekti).FirstOrDefault();
                var inspektime = dokumenta.Inspektimes.ToList();
                dynamic model = new ExpandoObject();
                model.inspektime = inspektime;
                return PartialView(model);
            }
        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: LlojDokumenti
        * Arsyeja: Per te afishuar partialView per skedaret e llojeve
        * Pershkrimi: Merret klasa model e llojit te dokumentit. E kalon ne nje liste dhe i afishon ne partial view perkatese.
        * Parametrat:string inspektimi
        * Return: PartialView bazuar ne model dinamik. 
        **/
        public ActionResult LlojDokumenti(string inspektimi)
        {
            using (var ac = new DetArkiveDatabaseContext())
            {
                var llojdokumenti = ac.LlojDokuments.ToList();

                dynamic model = new ExpandoObject();
                model.llojdokumenti = llojdokumenti;
                return PartialView(model);
            }
        }

        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: Documents
        * Arsyeja: Per te afishuar partialView per dokumentat e arkives
        * Pershkrimi: Merret klasa model te llojDokument, subjekt, Inspektim dhe Dokument. E kalon ne nje liste dhe i afishon ne partial view perkatese.
        * Parametrat:string subjekti, string inspektimi, string llojDok
        * Return: PartialView bazuar ne model dinamik. 
        **/
        public ActionResult Documents(string subjekti, string inspektimi, string llojDok)
        {
            using (var ac = new DetArkiveDatabaseContext())
            {
                var dokument = ac.Dokuments.Include(s => s.Subjekti).Include(s => s.Inspektime).Include(s => s.LlojDokument).ToList();
                var filtrimdok = new List<Dokument>();
                foreach (var d in dokument)
                {
                    if (d.Subjekti.Emer == subjekti && d.Inspektime.Emer == inspektimi && d.LlojDokument.Emer == llojDok)
                    {
                        filtrimdok.Add(d);
                    }
                    string docNameTmp;
                    if (d.Emer.Length > 8)
                    {
                        docNameTmp = d.Emer.Substring(0, 8) + "... ";
                    }
                    else
                    {
                        docNameTmp = d.Emer;
                    }
                }

                dynamic model = new ExpandoObject();
                model.filtrimdok = filtrimdok;
                return PartialView(model);
            }
        }

        public ActionResult Subjekt()
        {
            using (var ac = new DetArkiveDatabaseContext())
            {
                var subjektet = ac.Subjekt.ToList();

                dynamic model = new ExpandoObject();
                model.subjektet = subjektet;
                return PartialView(model);
            }
        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda:UploadDocuments
        * Arsyeja:Ngarkimi i dokumentave te arkives
        * Pershkrimi:Pasi merren te dhenat nga forma e deguar me ajax kontrollohen dokumentat qe do te uplodohen dhe me pas dokumentat shtohen ne databaze
        * Merret forma e derguar me ajax. Kontrollohen parametrat e dokumentit dhe kryet ngarkimi.
        * Return:Rikthehet repsone e tipt string ne requestin qe e therriti
        **/

        [HttpPost]
        public ActionResult Upload()
        {


            //Kushti qe aplikohet kur dokumenti eshte ngjedhur per tu ngarkuar.
            if (Request.Files.Count > 0)
            {
                var dokValiduar = Request.Form["emerValid"].Split(',');
                var docType = Request.Form["lloj"];
                var tage = Request.Form["index"].Split(',').ToList();
                var office = Request.Form["zyra"];
                var boxNr = Request.Form["nrKuti"];
                var rafti = Request.Form["rafti"];
                var subjektGlob = Request.Form["subjektGlob"];
                var inspektimGlob = Request.Form["inspektimGlob"];
                try
                {
                    //  Marrja e te gjitha dokumentave
                    HttpFileCollectionBase files = Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        fname = file.FileName;

                        if (Array.Exists(dokValiduar, el => el.CompareTo(fname) == 0))
                        {

                            //dokumentet ne byte
                            byte[] docInByte;
                            using (Stream inputStream = file.InputStream)
                            {
                                MemoryStream memoryStream = inputStream as MemoryStream;
                                if (memoryStream == null)
                                {
                                    memoryStream = new MemoryStream();
                                    inputStream.CopyTo(memoryStream);
                                }
                                docInByte = memoryStream.ToArray();
                            }

                            using (var ac = new DetArkiveDatabaseContext())
                            {
                                var session = Session["Emer"];
                                var indexDbNames = ac.Tags.Select(s => s.Emer).ToList();
                                var indexToBeInserted = tage.Except(indexDbNames).ToList();

                                foreach (var name in indexToBeInserted)
                                {
                                    ac.Tags.Add(new Tags { Emer = name }); //krijohen indekset dhe futen ne databaze
                                }
                                ac.SaveChanges();

                                var indexList = new List<Tags>();


                                foreach (var name in tage)
                                {
                                    indexList.Add(ac.Tags.Where(s => s.Emer == name).FirstOrDefault());
                                }


                                var userDb = ac.Perdoruesis.Where(s => s.Emer == session).FirstOrDefault();
                                var subjektGlobDatabase = ac.Subjekt.Where(s => s.Emer == subjektGlob).FirstOrDefault();
                                var inspektimGlobDatabase = ac.Inspektimes.Where(s => s.Emer == inspektimGlob).FirstOrDefault();
                                var llojdocDatabase = ac.LlojDokuments.Where(s => s.Emer == docType).FirstOrDefault();

                                //krijimi i dokumentit dhe me pas ruajtja e tij ne databaze 
                                Dokument dokument = new Dokument
                                {
                                    Emer = Path.GetFileNameWithoutExtension(fname),
                                    Tipi = Path.GetExtension(fname),
                                    Zyra = office,
                                    Kutia = int.Parse(boxNr),
                                    Rafti = int.Parse(rafti),
                                    Inspektime = inspektimGlobDatabase,
                                    Subjekti = subjektGlobDatabase,
                                    LlojDokument = llojdocDatabase,
                                    DateRregjistirimi = DateTime.Now,
                                    Perdoruesi = userDb,
                                    Tags = indexList,
                                    Permbajtja = docInByte
                                };
                                ac.Dokuments.Add(dokument);
                                ac.SaveChanges();
                            }
                        }

                    }
                    return Json("Dokumentet u ngarkuan me sukses.");
                }


                catch (Exception ex)
                {
                    return Json("Exception: " + ex.Message);
                }
            }


            else
            {
                return Json("Nuk ka dokumente te zgjedhura");
            }
        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda:DeleteF
        * Arsyeja:Fshirja e Dokumentave nga arkiva
        * Parametrat:string dokID
        * Return:Kthen Content string ne te njejten request e cila u thirr.
        **/
        [HttpPost]
        public ActionResult DeleteF(string dokID)
        {
            var idNeString = dokID.Split(',');
            var docId = int.Parse(idNeString[0]);
            using (var ac = new DetArkiveDatabaseContext())
            {
                var fshiFile = ac.Dokuments.Where(s => s.ID_Dokument == docId).FirstOrDefault();
                ac.Dokuments.Remove(fshiFile);
                ac.SaveChanges();
                return Content("success");
            }
        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda:DeleteFAuthenticate
        * Arsyeja:Autentikim ne menyre qe nje perdorues te fshij vetem dokumentat e tij.
        * Parametrat:string dokID
        * Return:Kthen Content string ne baze te kushteve, ne te njejten request e cila u thirr.
        **/
        [HttpPost]
        public ActionResult DeleteFAuthenticate(string dokID)
        {
            var idAsString = dokID.Split(',');
            var docId = int.Parse(idAsString[0]);
            using (var ac = new DetArkiveDatabaseContext())
            {
                var document = ac.Dokuments.Include(s => s.Perdoruesi).Where(s => s.ID_Dokument == docId).FirstOrDefault();
                if (document != null)
                {
                    if (document.Perdoruesi.Emer.CompareTo(Session["username"]) == 0) ////////////////////////////////////////////////////////////////////////////////
                    {
                        return Content("isAble");
                    }
                    else
                    {
                        return Content("notAble");
                    }
                }
                else
                {
                    return Content("notAble");
                }

            }

        }

        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: EditF
        * Arsyeja: Per te ndryshuar detajet e dokumentit . 
        * Return:Kthen Content string ne te njejten request e cila u thirr. 
        **/
        public ActionResult EditF()
        {
            var dokID = Request.Form["dokID"];
            var docId = int.Parse(dokID);
            var tage = Request.Form["indexdoc"].Split(',').ToList();
            var office = Request.Form["office"];
            var boxNr = Request.Form["boxNr"];
            var rafti = Request.Form["shelf"];

            using (var ac = new DetArkiveDatabaseContext())
            {
                var document = ac.Dokuments.Include(s => s.Tags).Where(s => s.ID_Dokument == docId).FirstOrDefault();
                document.Tags = new List<Tags>();

                var indexDbNames = ac.Tags.Select(s => s.Emer).ToList();
                var indexToBeInserted = tage.Except(indexDbNames).ToList();

                foreach (var name in indexToBeInserted)
                {
                    ac.Tags.Add(new Tags { Emer = name });
                }
                ac.SaveChanges();

                var indexList = new List<Tags>();

                foreach (var name in tage)
                {
                    indexList.Add(ac.Tags.Where(s => s.Emer == name).FirstOrDefault());
                }
                document.Tags = indexList;
                document.Zyra = office;
                document.Kutia = int.Parse(boxNr);
                document.Rafti = int.Parse(rafti);
                ac.SaveChanges();

            }
            return Content("success");
        }

        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: PreviewF
        * Arsyeja: Per te shikuar dokumentin. 
        * Parametrat:string dokID
        * Return:Kthen Content string ne te njejten request e cila u thirr.
        **/
        public ActionResult PreviewF(string dokID)
        {
            int docId = int.Parse(dokID);
            using (var ac = new DetArkiveDatabaseContext())
            {
                var document = ac.Dokuments.Include(s => s.Tags).Where(s => s.ID_Dokument == docId).FirstOrDefault();
                var indexNames = from i in document.Tags
                                 select i.Emer;
                var indexNamesString = indexNames.ToList();
                var objectToBeSent = new { Emri = document.Emer, Tipi = document.Tipi, Id = document.ID_Dokument, Zyra = document.Zyra, NrKutise = document.Kutia, Rafti = document.Rafti, Tags = indexNamesString };
                return Json(new { data = objectToBeSent }, JsonRequestBehavior.AllowGet);

            }

        }

        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: ShikoF
        * Arsyeja: Per te shikuar dokumentin. 
        * Parametrat:string dokID
        * Return:Kthen Content string ne te njejten request e cila u thirr.
        **/
        public ActionResult ShikoF(int dokID)
        {
            using (var ac = new DetArkiveDatabaseContext())
            {
                var document = ac.Dokuments.Where(s => s.ID_Dokument == dokID).FirstOrDefault();
                if (document.Tipi.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase) || document.Tipi.Equals(".txt", StringComparison.InvariantCultureIgnoreCase))
                {
                    return File(document.Permbajtja, document.Emer + document.Tipi);
                }
                else
                {
                    return Content("Ky format nuk suportohet.");
                }

            }
        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: DownloadF
        * Arsyeja: Per te shkarkuar dokumentin. 
        * Parametrat:string subjektGlob, string inspektimGlob, string docType, string downloadID. 
        * Return:Kthen Content byteArray ne te njejten request e cila u thirr.
        **/
        public ActionResult DownloadF(string subjektGlob, string inspektimGlob, string docType, string downloadID)
        {
            string downloadString = downloadID.Substring(0, downloadID.Length - 1);
            var eID = downloadString.Split(',');
            var eIDInteger = new List<int>();
            for (int i = 0; i < eID.Length; i++)
            {
                eIDInteger.Add(int.Parse(eID[i]));
            }



            if (subjektGlob != "" && inspektimGlob != "" && docType != "" && eID.Length == 1)
            {
                using (var ac = new DetArkiveDatabaseContext())
                {
                    var document = ac.Dokuments.Where(s => eIDInteger.Contains(s.ID_Dokument)).FirstOrDefault();
                    return File(document.Permbajtja, System.Net.Mime.MediaTypeNames.Application.Octet, document.Emer + document.Tipi);
                }

            }
            else
            {
                byte[] docBytes = DownloadFZip(subjektGlob, inspektimGlob, docType, eIDInteger);
                return File(docBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "Dokumenta Arkive.zip");
            }



        }


        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: DownloadFZip
        * Arsyeja: Per te shkarkuar disa dokumenta. 
        * Parametrat:string subjektGlob, string inspektimGlob, string docType
        * Return:Kthen ZipFile.
        **/
        private byte[] DownloadFZip(string subjektGlob, string inspektimGlob, string docType, List<int> eIDInteger)
        {
            List<Dokument> documents = new List<Dokument>();

            using (var ac = new DetArkiveDatabaseContext())
            {
                if (subjektGlob != "" && inspektimGlob != "" && docType != "")
                {
                    documents = ac.Dokuments.Where(s => eIDInteger.Contains(s.ID_Dokument)).ToList();
                }
                else if (subjektGlob != "" && inspektimGlob != "" && docType == "")
                {
                    int insectationId = int.Parse(inspektimGlob);
                    documents = ac.Dokuments.Include(s => s.LlojDokument)
                               .Include(s => s.Tags).Where(s => eIDInteger.Contains(s.LlojDokument.ID_LlojDokument))
                               .Where(s => s.Inspektime.ID_Inspektime == insectationId).ToList();
                }
                else if (subjektGlob != "" && inspektimGlob == "" && docType == "")
                {
                    documents = ac.Dokuments.Include(s => s.Inspektime).Where(s => eIDInteger.Contains(s.Inspektime.ID_Inspektime)).ToList();
                }
                else
                {
                    documents = ac.Dokuments.Include(s => s.Subjekti).Where(s => eIDInteger.Contains(s.Subjekti.ID_Subjekti)).ToList();
                }
            }
            using (var compressedFilesStream = new MemoryStream())
            {
                using (var arkiveKompresuar = new ZipArchive(compressedFilesStream, ZipArchiveMode.Create, false))
                {
                    foreach (var doc in documents)
                    {
                        var krijoArkive = arkiveKompresuar.CreateEntry(doc.Emer + doc.Tipi);
                        using (var originalFileStream = new MemoryStream(doc.Permbajtja))
                        {
                            using (var ArkiveStream = krijoArkive.Open())
                            {
                                originalFileStream.CopyTo(ArkiveStream);
                            }
                        }
                    }
                }
                return compressedFilesStream.ToArray();
            }

        }












        /**
   * Data: 27/09/2020
   * Programuesi: Skerdi Skenderi
   * Metoda:FilterDocuments
   * Arsyeja:Filtrimi i dokumentave
   * Pershkrimi:behet filtrimi i dokumentave ne base te te dhenave qe merren nga forma
   * Return:Rikthehet Content 
   **/
        public ActionResult FilterDocuments()
        {
            var emerSubjekti = Request.Form["emerSubjekti"];
            var nrInspektim = Request.Form["nrInspektim"];
            var llojDocument = Request.Form["llojDocument"];
            var emerDocument = Request.Form["emerDocument"];
            var tags = Request.Form["tags"];
            var zyre = Request.Form["zyre"];
            var kuti = Request.Form["kuti"];
            var raft = Request.Form["raft"];
            var dataNga = Request.Form["ngaData"];
            var dataTek = Request.Form["tekData"];

            List<Dokument> documents = ResultateFiltrimi(
            emerSubjekti,
            nrInspektim,
            llojDocument,
            emerDocument,
            tags,
            zyre,
            kuti,
            raft,
            dataNga,
            dataTek);


            return Content(SkedareDokumenteFiltrimi(documents).ToString());
        }

        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda:SkedareDokumenteFiltrimi
        * Arsyeja:Filtrimi i dokumentave ne string 
        * Pershkrimi:Formohen dive perkatese duke perdorur listen e dokumentave te filtruara
        * Parametrat:List<Dokument> documents
        * Return:Rikthehet response string me dokumentat mbas filtrimit, 
        **/

        private StringBuilder SkedareDokumenteFiltrimi(List<Dokument> documents)
        {

            StringBuilder response = new StringBuilder();
            foreach (var document in documents)
            {
                string docNameTmp;
                if (document.Emer.Length > 8)
                {
                    docNameTmp = document.Emer.Substring(0, 8) + "... ";
                }
                else
                {
                    docNameTmp = document.Emer;
                }
                response.AppendLine("<div class=\"inlinineLink\">");
                response.AppendLine("<input type = \"checkbox\" id =\"checkboxDocumentId" + document.ID_Dokument + "\" " + "name=\"nameDokument\" onchange=\"docCheckFilter(" + document.ID_Dokument + ",this.checked)\"/>");
                response.AppendLine("<div><img style = \"width:75px;height:85px\" src = \"/Imazhe/dokumentfilepic.png\"/>");
                response.AppendLine("<span id=\"spanDocumentId" + document.ID_Dokument + "\"" + ">" + docNameTmp + "" + document.Tipi + "</span></div>");
                response.AppendLine("</div>");
            }


            return response;
        }
        /**
        * Data: 18/11/2020
        * Programuesi: Skerdi Skenderi
        * Metoda: ResultateFiltrimi
        * Arsyeja: Filtrimi i dokumentave te arkives.
        * Parametrat:string subjektGlob, string inspektimGlob, string docType
        * Return:Nje liste me dok te filtruara
        **/
        private List<Dokument> ResultateFiltrimi(string emerSubjekti,
            string nrInspektim,
            string llojDocument,
            string emerDocument,
            string tags,
            string zyre,
            string kuti,
            string raft,
            string dataNga,
            string dataTek)
        {
            using (var ac = new DetArkiveDatabaseContext())
            {
                var documents = ac.Dokuments.Include(s => s.Subjekti).Include(s => s.Inspektime).Include(s => s.LlojDokument).Include(s => s.Tags).ToList();
                List<Dokument> documentPerkohshem = new List<Dokument>();
                //kontrolli per emrin e subjek
                if (emerSubjekti != "")
                {
                    documentPerkohshem = documents.Where(s => s.Subjekti.Emer.ToLower().Contains(emerSubjekti.ToLower())).ToList();
                    documents = documentPerkohshem;
                }
                //Kushti per inspektime
                if (nrInspektim != "")
                {
                    documentPerkohshem = documents.Where(s => s.Inspektime.Emer.ToLower().Contains(nrInspektim.ToLower())).ToList();
                    documents = documentPerkohshem;
                }
                //Kushti per llojedokumenti
              /*  if (llojDocument != "")
                {
                    var docType = int.Parse(llojDocument);
                    documentPerkohshem = documents.Where(s => s.LlojDokument.ID_LlojDokument == docType).ToList();
                    documents = documentPerkohshem;
                } */

                //Kushti per emer dokumenti
                if (emerDocument != "")
                {
                    documentPerkohshem = documents.Where(s => s.Emer.ToLower().Contains(emerDocument.ToLower())).ToList();
                    documents = documentPerkohshem;
                }



                //Kushti per Zyren
                if (zyre != "")
                {
                    documentPerkohshem = documents.Where(s => s.Zyra.ToLower().Contains(zyre.ToLower())).ToList();
                    documents = documentPerkohshem;
                }



                //Kushti per NrEkutise
                if (kuti != "")
                {
                    documentPerkohshem = documents.Where(s => s.Kutia == int.Parse(kuti)).ToList();
                    documents = documentPerkohshem;
                }



                //Kushti per NrERaftit
                if (raft != "")
                {
                    documentPerkohshem = documents.Where(s => s.Rafti == int.Parse(raft)).ToList();
                    documents = documentPerkohshem;
                }



                //Kushti per Indeksimet
                if (tags != "")
                {
                    var indeksimet = tags.Split(',').ToList();

                    documentPerkohshem = documents.Where(s =>
                    {
                        List<string> indexNames = new List<string>();
                        foreach (var ind in s.Tags)
                        {
                            indexNames.Add(ind.Emer);
                        }

                        return indexNames.Intersect(indeksimet).Count() != 0;
                    })
                    .ToList();
                    documents = documentPerkohshem;
                }


                //Kushti per fillimin e dates
                if (dataNga != "")
                {
                    CultureInfo cultureInfo = new CultureInfo("fr-FR");
                    DateTime ngaData = Convert.ToDateTime(dataNga, cultureInfo);
                    documentPerkohshem = documents.Where(s => s.DateRregjistirimi >= ngaData).ToList();
                    documents = documentPerkohshem;
                }



                //kushti per mbarimin e dates
                if (dataTek != "")
                {
                    CultureInfo cultureInfo = new CultureInfo("fr-FR");
                    DateTime tekData = Convert.ToDateTime(dataTek, cultureInfo);
                    documentPerkohshem = documents.Where(s => s.DateRregjistirimi <= tekData).ToList();
                    documents = documentPerkohshem;
                }

                return documents;
            }
        }

    }
}