﻿/**
* Versioni: 1.0.0
* Data: 28/09/2020
* Programuesi:  Skerdi Skenderi
* Pershkrimi: Klase kontroller per menaxhimin e funksionaliteteve te funksioneve te Login.
**/

using DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using DetyraArkives_SkerdilajdSkenderi.Models;
using System.Security.Cryptography.X509Certificates;

namespace DetyraArkives_SkerdilajdSkenderi.Controllers
{
/**
* Data: 28/09/2020
* Programuesi: Skerdi Skenderi
* Klasa: ArchiveControlller
* Arsyeja:Menaxhon kerkesat qe fijne nga Index.cshtml(Login)
* Trashegon nga: Controller
**/
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();

        }
        [HttpPost]
        public ActionResult Index(Perdoruesi p)
        {

            using (DetArkiveDatabaseContext db = new DetArkiveDatabaseContext())
            {
                var obj = db.Perdoruesis.Where(u => u.Emer.Equals(p.Emer) && u.Fjalekalim.Equals(p.Fjalekalim)).FirstOrDefault();
                if (obj != null)
                {
                    Session["ID_Perdoruesi"] = obj.ID_Perdoruesi.ToString();
                    Session["Emer"] = obj.Emer.ToString();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Emri i perdoruesit ose fjalekalimi nuk eshte i sakte.");
                }
            }

            return View();
        }
    }
}