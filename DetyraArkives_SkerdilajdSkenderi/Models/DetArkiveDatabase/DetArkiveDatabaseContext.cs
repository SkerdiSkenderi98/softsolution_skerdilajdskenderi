﻿/**
* Versioni: 1.0.0
* Data: 21/09/2020
* Programuesi: Skerdi Skenderi
* Pershkrimi: Klase konteksti per db ku kalohen te gjitha klasat model ne DBSet per EntityFramework
**/
using DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase;
using System.Data.Entity;

namespace DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase

{
    public class DetArkiveDatabaseContext : DbContext
    {
        public DetArkiveDatabaseContext() : base("Filtra")
        {

        }

        public DbSet<Inspektime> Inspektimes { get; set; }
        public DbSet<Dokument> Dokuments { get; set; }
        public DbSet<Subjekti> Subjekt { get; set; }
        public DbSet<Perdoruesi> Perdoruesis { get; set; }
        public DbSet<Tags> Tags { get; set; }
        public DbSet<LlojDokument> LlojDokuments { get; set; }



    }
}