﻿/**
* Versioni: 1.0.0
* Data: 21/09/2020
* Programuesi: Skerdi Skenderi
* Pershkrimi: File permban metoda get/set per krijimin e nje tabele Tags (Nr Indeksimit) ne databaze.
**/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase
{
    public class Tags
    {
        public int ID { get; set; }
        public string Emer { get; set; }
        public List<Dokument> Dokuments { get; set; }
    }

}
/**
* Versioni: 1.0.0
* Data:
* Programuesi: Skerdi Skenderi
* Pershkrimi: Pershkrim i pergjithshem cfare funksionalitetesh permban file
**/