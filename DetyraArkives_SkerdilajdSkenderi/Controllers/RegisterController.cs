﻿/**
* Versioni: 1.0.0
* Data: 28/09/2020
* Programuesi:  Skerdi Skenderi
* Pershkrimi: Klase kontroller per menaxhimin e funksionaliteteve te funksioneve te Rregjistrimit.
**/

using DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using DetyraArkives_SkerdilajdSkenderi.Models;
using System.Security.Cryptography.X509Certificates;

namespace DetyraArkives_SkerdilajdSkenderi.Controllers
{
/**
* Data: 28/09/2020
* Programuesi: Skerdi Skenderi
* Klasa: ArchiveControlller
* Arsyeja:Menaxhon kerkesat qe fijne nga Index.cshtml(Register)
* Trashegon nga: Controller
**/
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Perdoruesi p)
        {
            Perdoruesi per = new Perdoruesi();
            per.Emer = p.Emer;
            per.Email = p.Email;
            per.Fjalekalim = p.Fjalekalim;

            if (ModelState.IsValid)
            {
                {
                    using (DetArkiveDatabaseContext db = new DetArkiveDatabaseContext())
                    {
                        db.Perdoruesis.Add(per);
                        db.SaveChanges();
                    }
                    ModelState.Clear();

                }
                return RedirectToAction("Index", "Login");
            }

            return RedirectToAction("Index", "Register");
        }
    }
}