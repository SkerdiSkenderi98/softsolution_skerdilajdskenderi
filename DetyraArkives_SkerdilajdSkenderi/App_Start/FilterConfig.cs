﻿using System.Web.Mvc;

namespace DetyraArkives_SkerdilajdSkenderi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
