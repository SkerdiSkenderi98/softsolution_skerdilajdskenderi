﻿/**
* Versioni: 1.0.0
* Data: 21/09/2020
* Programuesi: Skerdi Skenderi
* Pershkrimi: File permban metoda get/set per krijimin e nje tabele Subjekti ne databaze, ne lidhje me klasat e Inspektime dhe Dokument
**/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase
{
    public class Subjekti
    {
        [Key]
        public int ID_Subjekti { get; set; }
        public String Emer { get; set; }

        public List<Inspektime> Inspektimes {get; set;}
        public List<Dokument> Dokuments { get; set; }

    }
}