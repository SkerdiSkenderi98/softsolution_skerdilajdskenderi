﻿/**
* Versioni: 1.0.0
* Data: 17/09/2020
* Programuesi:Skerdi Skenderi
* Pershkrimi: Klase JS ne te cilen ndodhen funksionet ne lidhje me faqjen kryesore.
**/

//deklarimi i disa variablave globale te cilat do perdoren ne file
var subjektGlob = "";
var inspektimGlob = "";
var llojGlob = "";
var startDateFilter = true;
var endDateFilter = true;
var filterAktiv = false;
var checkboxAktiv = "";

//Funksion per afishimin e skedareve te Inspektimeve
function afishoInspektime(subjekti) {
    subjektGlob = subjekti;
    var lista = "<li class='breadcrumb-item'><a style='color:#4582ec;cursor:pointer' onclick=afishoInspektimeLink(this.innerHTML)>" + subjekti + "</a></li>";
    $("#LinkNav").append(lista);

    var dataToBeSent = "subjekti=" + subjekti;

    $.ajax({
        type: "Get",
        url: "/home/Inspektime",
        data: dataToBeSent,
        success: function (response) {
            document.getElementById("filet").innerHTML = response;
        }
    });
}



//Funksion per afishimin e skedareve te Llojit
function afishoLlojeDokumenti(inspektimi) {
    inspektimGlob = inspektimi;
    var lista = "<li class='breadcrumb-item'><a style='color:#4582ec;cursor:pointer' onclick=afishoLlojeDokumentiLink(this.innerHTML)>" + inspektimi + "</a></li>";
    $("#LinkNav").append(lista);

    var dataToBeSent = "inspektimi=" + inspektimi;

    $.ajax({
        type: "Get",
        url: "/home/LlojDokumenti",
        data: dataToBeSent,
        success: function (response) {
            document.getElementById("filet").innerHTML = response;
        }
    });
}

//Funksion per afishimin e skedareve te dokumentave te ruajtur
function afishoDokumenta(lloj) {
    llojGlob = lloj;
    var lista = "<li class='breadcrumb-item'><a href='#'>" + lloj + "</a></li>";
    $("#LinkNav").append(lista);

    var dataToBeSent = "subjekti=" + subjektGlob + "&inspektimi=" + inspektimGlob + "&llojDok=" + llojGlob;

    $.ajax({
        type: "Get",
        url: "/home/Documents",
        data: dataToBeSent,
        success: function (response) {
            document.getElementById("filet").innerHTML = response;
        }
    });
}

//Funksione per navigimin e breadcrumb (kthim pas)
function afishoSubjekteLink() {
    var parent = document.getElementById("LinkNav");
    while (parent.childElementCount > 1) {
        parent.removeChild(parent.lastChild);
    }
    inspektimGlob = "";
    llojGlob = "";
    subjektGlob = "";
    afishoSubjekte();

}
function afishoSubjekte() {

    $.ajax({
        type: "Get",
        url: "/home/Subjekt",
        success: function (response) {
            document.getElementById("filet").innerHTML = response;
        }
    });
}

function afishoInspektimeLink(subjekti) {
    var parent = document.getElementById("LinkNav");
    while (parent.childElementCount > 1) {
        parent.removeChild(parent.lastChild);
    }
    inspektimGlob = "";
    llojGlob = "";
    subjektGlob = subjekti;
    afishoInspektime(subjekti);

}

function afishoLlojeDokumentiLink(inspektimi) {
    var parent = document.getElementById("LinkNav");
    while (parent.childElementCount > 2) {
        parent.removeChild(parent.lastChild);
    }
    inspektimGlob = inspektimi;
    llojGlob = "";
    afishoLlojeDokumenti(inspektimi);

}

function afishoDokumentaLink(lloj) {
    var parent = document.getElementById("LinkNav");
    while (parent.childElementCount > 3) {
        parent.removeChild(parent.lastChild);
    }
    afishoDokumenta(lloj);

}


/*
* Funksione per checkboxet e Arkives
*/


/*
 *Selektimi ose deselektimi i checkboxeve kur klikohet selectAll
 * */
function checkSelect() {
    var checkboxes = document.querySelectorAll("input[type='checkbox']");
    if (document.getElementById("selectAll").checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = true;
            if (checkboxes[i].id != document.getElementById("selectAll").id) {
                var id = "#" + checkboxes[i].id;
                $(id).trigger("onchange");
            }
        }
    }
    else {
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = false;
            if (checkboxes[i].id != document.getElementById("selectAll").id) {
                var id = "#" + checkboxes[i].id;
                $(id).trigger("onchange");
            }
        }
    }
}



/**
 * Funksioni qe ndjek dokumentat e checkuar ne rast se kemi ber filtrim 
 * @param {any} id
 * @param {any} checked
 */
function docCheckFilter(id, checked) {
    filterAktiv = true;
    documentCheckChanged(id, checked);
}
/**
 * Funksoni qe ndjek dokumentat e checkuar
 * @param {any} id
 * @param {any} checked
 */
function documentCheckChanged(id, checked) {
    checkSelectAll(); //fshire per momentin
    if (checked) {
        var idAsString = id + "";
        if (!checkboxAktiv.includes(idAsString)) {
            checkboxAktiv += id + ",";
        }
    }
    else {
        var stringToRemove = id + ",";
        checkboxAktiv = checkboxAktiv.replace(stringToRemove, "");
    }

    fshiAuthenticate();//kontrolli nqs ky perdorues ka mundesi te fshij dokumentin

    //kontrolli per shfaqjen e butonit te shkarkimit


    var count = 0;
    for (var i = 0; i < checkboxAktiv.length; i++) {
        if (checkboxAktiv[i] == ',') {
            count++;
        }
    }
    //kontrolli per shfaqjen e butonit te preview
    if (count == 1) {
        document.getElementById("preview").disabled = false;
    }
    else {
        document.getElementById("preview").disabled = true;
    }

}


/**
 * Funksioni qe ndjek folderat e checkuar
 * @param {any} id
 * @param {any} checked
 */
function FolderCheckChanged(id, checked) {
    checkSelectAll(); //fshire per momentin
    if (checked) {
        var idAsString = id + "";
        if (!checkboxAktiv.includes(idAsString)) {
            checkboxAktiv += id + ",";
        }
    }
    else {
        var stringToRemove = id + ",";
        checkboxAktiv = checkboxAktiv.replace(stringToRemove, "");
    }

    if (checkboxAktiv != "") {
        document.getElementById("download").disabled = false;
    }
    else {
        document.getElementById("download").disabled = true;
    }
}


function checkSelectAll() {

    var allBoxes = document.querySelectorAll("input[type='checkbox']");
    var checked = 0;// numri te selektuarave
    var unchecked = 0;// numri i te paselektuarave
    for (var i = 0; i < allBoxes.length; i++) {
        if (allBoxes[i].checked) {
            checked++;
        }
        else {
            unchecked++;
        }
        var selectAllCheckbox = document.getElementById("selectAll");
        if (checked == allBoxes.length - 1 && !selectAllCheckbox.checked) {//nsq ka ngelur vetem selectAll pa u checkuar
            document.getElementById("selectAll").checked = true;
        }
        else if (unchecked == allBoxes.length - 1 && selectAllCheckbox.checked) {//Nqs ka ngelur vtm checkboxi pa u uncheckuar
            document.getElementById("selectAll").checked = false;
        }
    }
}








/*
* Funksione per Upload dokumenti
*/

//Funksion per hapen e dritares per zgjedhjen e dokumentit.
function zgjithFile() {
    document.getElementById("uploadHidden").click();
}

//Funksion i cili hedh parametrat ne forme.
function uploadF() {
    var llojdoc = document.getElementById("llojdokumentiUpload").value;
    var indexdoc = document.getElementById("indexUpload").value;
    var nrKuti = document.getElementById("nrKutiUpload").value;
    var rafti = document.getElementById("raftiUpload").value;
    var zyra = document.getElementById("zyraUpload").value;
    var emerValid = document.getElementById("documentNamesUpload");



    var ArrayemerValiduar = [];
    for (var i = 0; i < emerValid.childElementCount; i++) {
        var emerDocSpan = emerValid.childNodes[i];
        ArrayemerValiduar[i] = emerDocSpan.childNodes[i].innerHTML;
        if (/[&,]/.test(ArrayemerValiduar[i])) {
            document.getElementById("uploadFormError").innerHTML = "Nuk lejohet te ngarkohen dokumente me karaktere speciale. <br> P.Sh: [*,?/\":]";
            return;
        }
    }

    var uploadFile = $("#uploadHidden").get(0);
    var files = uploadFile.files;

    //Krijimi i nje formData per parametrat e hedhur.
    var fileData = new FormData();

    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }

    fileData.append("emerValid", ArrayemerValiduar);
    fileData.append("index", indexdoc);
    fileData.append("lloj", llojdoc);
    fileData.append("nrKuti", nrKuti);
    fileData.append("rafti", rafti);
    fileData.append("zyra", zyra);
    fileData.append("subjektGlob", subjektGlob);
    fileData.append("inspektimGlob", inspektimGlob);

    // Dergoj formen me Ajax
    $.ajax({
        url: '/Home/Upload',
        type: "POST",
        contentType: false,
        processData: false,
        data: fileData,
        success: function (result) {
            afishoDokumenta(llojdoc);
        },
        error: function (err) {
            alert(err.statusText);
        }
    });
}

function zgjidhDok() {
    document.getElementById("uploadFormError").innerHTML = "";
    document.getElementById("zgjithFile").click();

}


function hiqDocEmer(idSpan) {
    var hiqDoc = document.getElementById(idSpan);
    hiqDoc.parentNode.removeChild(hiqDoc)
}

//Funksion i cili merr emrin e dokumentit qe ngarkohet dhe afishon skedare ne spane perkatese.
function getDocEmer() {
    document.getElementById("documentNamesUpload").innerHTML = "";
    var doc = document.getElementById("uploadHidden");
    for (var i = 0; i < doc.files.length; i++) {
        var emerDocSpan = "emerDocSpan" + i;
        var elementIndex = "<span id =" + emerDocSpan + " class =\"documentNameSpan\"><span>" + doc.files.item(i).name + "</span><span class =\" badge badge-light badge-pill\" onclick=\"hiqDocEmer('" + emerDocSpan + "')\"> X </span></span>";
        document.getElementById("documentNamesUpload").innerHTML += elementIndex;
    }
}




/*
* Funksione per fshirjen e dokumentave nga arkiva
*/

function fshiDok() {

    var dokID = "dokID=" + checkboxAktiv;
    $.ajax({
        url: "/Home/DeleteF",
        type: "POST",
        data: dokID,
        success: function (result) {
            afishoDokumenta(llojdoc);
            if (result == "success") {
                $('#successModal').modal('show');
              
                if (filterAktiv) {//nqs eshte fshire gjat filtrimit therrasim filterin
                    filter();
                }
                else {
                    openFileNav(subjektGlob[0], inspektimGlob[0], llojGlob[0]);//dergimi po ne ate direktori
                }

            }
        },
        error: function (err) {
            alert(err.statusText); ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }


    });
}

function fshiAuthenticate() {
    var count = 0;
    for (var i = 0; i < checkboxAktiv.length; i++) {
        if (checkboxAktiv[i] == ',') {
            count++;
        }
    }

    if (count != 1) {
        document.getElementById("fshi").disabled = true;
        return;
    }
    var dokID = "dokID=" + checkboxAktiv;

    //Perdorimi i Ajax per dergimin e ID te dokumentit tek DeleteFAuthenticate

    $.ajax({
        url: "/Home/DeleteFAuthenticate",
        type: "POST",
        data: dokID,
        success: function (result) {
            if (result == "isAble") {
                var counter = 0;
                for (var i = 0; i < checkboxAktiv.length; i++) {
                    if (checkboxAktiv[i] == ',') {
                        counter++;
                    }
                }
                if (counter != 1) {
                    document.getElementById("fshi").disabled = true;
                }
                else {
                    document.getElementById("fshi").disabled = false;
                }

            }
        },



    });
}

    /** 
    /**
     * Funksione per Preview/Editim e dokumentave te arkives
     **/

    // Funksion qe afishon modalin e previews
    function modalPreview() {

        var dokID = checkboxAktiv.substring(0, checkboxAktiv.length - 1);
        dokID = "dokID=" + dokID;
        document.getElementById("preview").disabled = true;
        $.ajax({
            url: "/Home/PreviewF",
            type: "POST",
            datatype: "JSON",
            data: dokID,
            success: function (result) {
                //Mbushet modali me te dhenat e dokumentit
                document.getElementById("zyraPreview").value = result.data.Zyra;
                document.getElementById("nrKutiPreview").value = result.data.NrKutise;
                document.getElementById("raftiPreview").value = result.data.Rafti;
                document.getElementById("docNamePreview").value = result.data.Emri + result.data.Tipi;
                document.getElementById("indexPreview").value = result.data.Tags; //result.data.Tags.forEach(el => $("#indexShiko").tagsinput('add', el));
                document.getElementById("iframePreview").src = "/Home/ShikoF?dokID=" + checkboxAktiv.substring(0, checkboxAktiv.length - 1);
                $("#previewDocument").modal("show");
               // document.getElementById("PreviewLoadIcon").className = "fa fa-eye";//spinner per loading
                //document.getElementById("preview").disabled = false;
            },
            error: function (err) {
                alert(err.statusText);
            }


        });


    }

    /** 
     * Funksioni per editimeDokumenti e dokumentit
    **/
    function EditF() {
        var dokID = checkboxAktiv.substring(0, checkboxAktiv.length - 1);
        var indexdoc = document.getElementById("indexPreview").value;
        var office = document.getElementById("zyraPreview").value;
        var boxNr = document.getElementById("nrKutiPreview").value;
        var shelf = document.getElementById("raftiPreview").value;
        if (indexdoc == "") {
            document.getElementById("editDocumentError").innerHTML = "Ju lutem plotësoni fushën e indekseve!";
            return;
        }
        //Krijohet forma dhe mbushet me te dhena
        var formData = new FormData();
        formData.append("dokID", dokID);
        formData.append("indexdoc", indexdoc);
        formData.append("office", office);
        formData.append("boxNr", boxNr);
        formData.append("shelf", shelf);

        $.ajax({
            url: "/Home/EditF",
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
            success: function (result) {
                if (result == "success") {
                    document.getElementById("successMessage").innerHTML = "Dokumenti u ndryshua me sukses!";
                   // $("#previewDocument").modal("hide");
                   // $('#successModal').modal('show');
                    if (filterAktiv) {
                        filter();
                    }
                    else {
                        openFileNav(subjektGlob[0], inspektimGlob[0], llojGlob[0]);
                    }

                }
            },
            error: function (err) {
                alert(err.statusText);
            }


        });

    }

    /**
     * Funksioni qe filtron dokumenta
     *
     * */


function filtriArkives() {
    var numerInspektimFilter = document.getElementById("numerInspektimFilterResult").value;
    var emerSubjektFilter = document.getElementById("emerSubjektFilterResult").value;
    var llojDokumentiFilter = document.getElementById("llojDokumentiFilterResult").value;
    var emerDokumentiFilter = document.getElementById("emerDokumentiFilterResult").value;
    var fusheIndeksFilter = document.getElementById("fusheIndeksFilterResult").value;
    var zyreFilter = document.getElementById("zyreFilterResult").value;
    var kutiaFilter = document.getElementById("kutiaFilterResult").value;
    var shelfFilter = document.getElementById("shelfFilterResult").value;
    var startDateFilter = document.getElementById("startDateFilterResult").value;
    var endDateFilter = document.getElementById("endDateFilterResult").value;

    //nqs shtypin butonin filtron me fushat bosh
    if (emerSubjektFilter == "" && numerInspektimFilter == "" && llojDokumentiFilter == "" && emerDokumentiFilter == "" && fusheIndeksFilter == "" && zyreFilter == "" && kutiaFilter == "" && shelfFilter == "" && startDateFilter == "" && endDateFilter == "") {
        clearFilter();
        return;
    }
    document.getElementById("LinkNav").hidden = true;
    checkboxAktiv = "";
   // document.getElementById("selectAll").checked = false;


    //Butoni ngarko caktivizohet
    document.getElementById("upload").disabled = false;


    //Butoni fshi caktivizohet
    document.getElementById("fshi").disabled = false;


    //Butoni shkarko caktivizohet
    document.getElementById("download").disabled = false;


    //Butoni preview/edit caktivizohet
    document.getElementById("preview").disabled = false;


    var formData = new FormData();//krijohet forma
    formData.append("emerSubjekti", emerSubjektFilter);
    formData.append("nrInspektim", numerInspektimFilter);
    formData.append("llojDocument", llojDokumentiFilter);
    formData.append("emerDocument", emerDokumentiFilter);
    formData.append("tags", fusheIndeksFilter);
    formData.append("zyre", zyreFilter);
    formData.append("kuti", kutiaFilter);
    formData.append("raft", shelfFilter);
    formData.append("ngaData", startDateFilter);
    formData.append("tekData", endDateFilter);
    document.getElementById("lista-skedare");
   document.getElementById("searchfilter").disabled = true;
   document.getElementById("clearfilter").disabled = true;
    $.ajax({
        url: "/Home/FilterDocuments",
        type: "POST",
        contentType: false,
        processData: false,
        data: formData,
        success: function (response) {
            var zeroResults = "<div style='margin-top:15%;margin-left:30%;font-size:2em;'>Nuk ka asnjë rezultat me këtë kërkim!<div>";//kur nuk ka rezultate
            if (response == "") {
                document.getElementById("lista-skedare").innerHTML = zeroResults;
                document.getElementById("searchfilter").disabled = false;
                document.getElementById("clearfilter").disabled = false;
            }


            else {
                document.getElementById("lista-skedare").innerHTML = response;
                document.getElementById("searchfilter").disabled = false;
                document.getElementById("clearfilter").disabled = false;
            }

        },
        error: function (err) {
            alert(err.statusText);
        }


    });
}
        //Funksioni qe ben pastrimin e fushave te filtrimit
        function clearFilter() {
          //  document.getElementById("FilterDocumentLocation").innerHTML = "";
            filterAktiv = false;// mbyll filtrin if false
            document.getElementById("LinkNav").hidden = false;
           // document.getElementById("selectAll").checked = false;

            //Butoni ngarko caktivizohet
            document.getElementById("upload").disabled = true;

            //Butoni fshi caktivizohet
            document.getElementById("fshi").disabled = true;

            //Butoni shkarko caktivizohet
            document.getElementById("download").disabled = true;

            //Butoni preview/edit caktivizohet
            document.getElementById("preview").disabled = true;

            /*Behet pastrimi fushave*/
            document.getElementById("emerSubjektFilterResult").value = "";
            document.getElementById("numerInspektimFilterResult").value = "";
            document.getElementById("llojDokumentiFilterResult").selected = true;
            document.getElementById("emerDokumentiFilterResult").value = "";
            document.getElementById("fusheIndeksFilterResult").value = "";
            document.getElementById("zyreFilterResult").value = "";
            document.getElementById("kutiaFilterResult").value = "";
            document.getElementById("shelfFilterResult").value = "";
            document.getElementById("startDateFilterResult").value = "";
            document.getElementById("endDateFilterResult").value = "";
            checkboxAktiv = "";
            subjektGlob[0] = "";
            subjektGlob[1] = "";
            inspektimGlob[0] = "";
            inspektimGlob[1] = "";
            llojGlob[0] = "";
            llojGlob[1] = "";
            lista-skedare();
        }


function DownloadF() {
    if (llojGlob[0] != "") {
        entitySearch = "dokument";
    }
    else if (inspektimGlob[0] != "") {
        entitySearch = "llojdokumenti";
    }
    else if (subjektGlob[0] != "") {
        entitySearch = "inspektime";
    }
    else {
        entitySearch = "subjektet";
    }
    var dataToBeSent = "";
    if (filterAktiv) {//nqs kemi filtruar dokumentat
        dataToBeSent = "subjektGlob=filter" + subjektGlob[0] + "&inspektimGlob=filter" + inspektimGlob[0] + "&lloj=filter" + llojGlob[0] + "&downloadID=" + checkboxAktiv;
    }
    else {
        dataToBeSent = "subjektGlob=" + subjektGlob[0] + "&inspektimGlob=" + inspektimGlob[0] + "&lloj=" + llojGlob[0] + "&downloadID=" + checkboxAktiv;
    }

    window.location = "/Home/DownloadF?" + dataToBeSent;
}



//Funksione per validimin e fushave te pranojne vetem numra
function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;

    return true;
} 

function InvalidMsg(textbox) {

    if (textbox.value === '') {
        textbox.setCustomValidity
            ('Ju lutem plotesoni fushen me informacionin perkates.');
    } else if (textbox.validity.typeMismatch) {
        textbox.setCustomValidity
            ('Informacioni nuk eshte ne formatin e duhur.');
    } else {
        textbox.setCustomValidity('');
    }

    return true;
} 

//Funksione kaledarike

$(document).ready(function () {

    $("#startDateFilterResult").keyup(function () {

        var reg = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/.test($(this).val());
        if (!reg) {
            startDateFilter = false;
        }
        else {
            startDateFilter = true;
        }
    });

    $("#startDateFilterResult").focusout(function () {

        if (!startDateFilter) {
            document.getElementById("startDateFilterResult").value = "";
            startDateFilter = true;
        }
    });



    $("#endDateFilterResult").keyup(function () {

        var reg = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/.test($(this).val());
        if (!reg) {
            endDateFilter = false;
        }
        else {
            endDateFilter = true;
        }
    });

    $("#endDateFilterResult").focusout(function () {

        if (!endDateFilter) {
            document.getElementById("endDateFilterResult").value = "";
            endDateFilter = true;
        }
    });



    $("#previewDocument").on("hidden.bs.modal", function (e) {
        $("#indexPreview").tagsinput("removeAll");
    });

    $("#previewDocument").on("hidden.bs.modal", function (e) {
        document.getElementById("editDocumentError").innerHTML = "";
    });

    //Perkthimi ne gjuhen shqipe i kalendarit
    $.fn.datepicker.dates['Albania'] = {
        days: ["E Diele", "E Hene", "E Marte", "E Merkure", "E Enjte", "E Premte", "E Shtune", "E Diele"],
        daysShort: ["Di", "Hen", "Mar", "Mer", "Enj", "Pre", "Sht", "Di"],
        daysMin: ["Die", "Hen", "Mar", "Mer", "Enj", "Pre", "Sht", "Die"],
        months: ["Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "Nentor", "Dhjetor"],
        monthsShort: ["Jan", "Shk", "Mar", "Pri", "Maj", "Qer", "Kor", "Gu", "Sht", "Tet", "Nen", "Dhj"],
        today: "Sot",
        clear: "Pastro"
    };
    $("#startDateFilterResult").datepicker({
        format: 'dd/mm/yyyy',
        language: 'Albania',
        weekStart: 1
    });
    $("#endDateFilterResult").datepicker({
        format: 'dd/mm/yyyy',
        language: 'Albania',
        weekStart: 1
    });
});