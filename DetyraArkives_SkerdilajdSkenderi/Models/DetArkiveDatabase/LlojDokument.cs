﻿/**
* Versioni: 1.0.0
* Data: 21/09/2020
* Programuesi: Skerdi Skenderi
* Pershkrimi: File permban metoda get/set per krijimin e nje tabele LlojDokument ne databaze.
**/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DetyraArkives_SkerdilajdSkenderi.Models.DetArkiveDatabase
{
    public class LlojDokument
    {
        [Key]
        public int ID_LlojDokument { get; set; }
        public string Emer { get; set; }
        public List<Dokument> Dokuments { get; set; }


    }
}